package s02;

import java.lang.Math;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите число Х: ");
        double x = input.nextDouble();
        double y;
        if (x > 0) y = Math.sin(x) * Math.sin(x);
        else y = 1 - 2 * Math.sin(Math.pow(x, 2));
        System.out.println("Значение y равно: " + y);
    }
}