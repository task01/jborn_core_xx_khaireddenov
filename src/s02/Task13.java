package s02;
import java.util.Scanner;

public class Task13 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите количество элементов массива ");
        int n = input.nextInt();
        System.out.println("Введите числа массива");
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        System.out.println("Заполненый массив: ");
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        double sum = 0;
        for (int i = 0; i < arr.length; i++) {
                sum = sum + arr[i];
        }
        System.out.println("Среднее арифметическое значение равно: " + sum /arr.length);
    }
}