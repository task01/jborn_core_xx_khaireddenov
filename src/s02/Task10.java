package s02;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите количество элементов в массиве: ");
        int command = s.nextInt();
        int numbers[] = new int[command];
        for (int i = 0; i < numbers.length; i++) {
            System.out.println("Введите" + " " + i + " " + "элемент массива");
            numbers[i] = s.nextInt();
        }
        int sum = 0;
        int mult = 1;
        for (int i = 0; i < numbers.length; i++) {
            if (i % 2 == 0) {
                mult *= numbers[i];
            } else {
                sum += numbers[i];
            }
        }
        System.out.println("Сумма четных элементов массива: " + sum);
        System.out.println("Произведение нечетных элементов массива: " + mult);
    }
}
