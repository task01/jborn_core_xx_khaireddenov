package s02;
import java.util.Scanner;

public class Task9 {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите число N: ");
            int i = scanner.nextInt();
            int sum = 0;
            while (i != 0) {
                sum += i % 10;
                i /= 10;
            }
            System.out.println("Сумма цифр равна: " + sum);
        }
}
