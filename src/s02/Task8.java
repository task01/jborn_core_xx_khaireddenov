package s02;

import java.util.Scanner;

public class Task8 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите число N: ");
        int n = in.nextInt();
        System.out.print("Таблица умножения для введенного числа \n");
        for (int i = 1; i < 10; i++) {
            System.out.println(n + " x " + (i + 0) + " = " +
                    (n * (i + 0)));
        }
    }
}


