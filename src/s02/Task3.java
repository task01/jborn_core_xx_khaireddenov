package s02;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите двухзначное число:");
        int chislo = scan.nextInt();
        int eden = chislo % 10; // кол-во единиц
        int des = chislo / 10; // кол-во десятков
        int summa = des + eden; // сумма цифр
        int proiz = des * eden; // произведение цифр
        System.out.println("Kоличество десятков:" + des + ".\nKоличество единиц:" + eden + ".\nCумма цифр:" + summa + ".\nПроизведение цифр:" + proiz + ".");
        scan.close();
    }
}
