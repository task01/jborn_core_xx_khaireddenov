package s02;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите число: ");
        int sec = in.nextInt();
        int s = sec % 60;
        int h = sec / 60;
        int m = h % 60;
        h = h / 60;
        System.out.print( h + ":" + m + ":" + s);
        System.out.print("\n");
    }
}
