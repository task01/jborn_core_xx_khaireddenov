package s02;
import java.util.Arrays;

public class Task11 {
    public static void main(String[] args) {
        int[] fall = new int[]{1, 9, 1, 8, 35, 80, 55, 25, 5, 4, 55, 99, 120, 130, 74, 68, 88, 55, 1, 147, 14, 18, 75, 89, 5, 3, 7, 187, 97, 12};
        int [] numbersCopy = Arrays.copyOf(fall, fall.length);
        System.out.print("Массив сведений по осадкам за 30 дней:  \n");
        System.out.println(Arrays.toString(numbersCopy));
        int min = fall[0], max = min;
        for (int i = 0; i < fall.length; i++) {
            if (fall[i] < min)
                min = fall[i];
            if (fall[i] > max)
                max = fall[i];
        }
        System.out.print("\n Максимальное значение осадков:  " + max+ "\n Произошло в следующие дни: ");
        for(int i = 0; i < fall.length; i++) {
            if(fall[i] == max)
                System.out.print((i + 1) + ", ");
        }
        System.out.print("\n Минимальное значение осадков:  " + min + "\n Произошло в следующие дни: ");
        for(int i = 0; i < fall.length; i++) {
            if(fall[i] == min)
                System.out.print((i + 1) + ", ");
        }
    }
}