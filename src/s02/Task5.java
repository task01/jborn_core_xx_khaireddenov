package s02;

import java.util.Scanner;
import java.util.Arrays;
import static java.lang.Math.pow;

public class Task5 {
    public static void main(String[] args) {
        System.out.println("Введите три целых числа: ");
        int[] arr = new int[3];
        arr[0] = requestNumber();
        arr[1] = requestNumber();
        arr[2] = requestNumber();
        Arrays.sort(arr);
        if ((pow(arr[0], 2) + pow(arr[1], 2)) == pow(arr[2], 2)) {
            System.out.println("Эти числа являются тройкой Пифагора");
        } else {
            System.out.println("Эти числа не являются тройкой Пифагора");
        }
    }
    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
