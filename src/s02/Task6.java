package s02;
import java.util.Scanner;

public class Task6 {
    static Scanner input = new Scanner(System.in);
    public static void main(String args[]) {
        System.out.print("Введите радиус:\n");
        int rad = input.nextInt();
        double s = Math.PI * Math.pow(rad, 2);
        System.out.println("Площадь круга: " + s);
        double c = 2 * Math.PI * rad;
        System.out.println("Длина окружности: " + c);
    }
}

