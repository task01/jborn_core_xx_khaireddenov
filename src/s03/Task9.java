package s03;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.println("Введите колличество строк: ");
        n = sc.nextInt();
        String[] str = new String[n];
        sc.nextLine();
        for (int i = 0; i < n; i++) {
            System.out.println("Введите строку №" + (i + 1));
            str[i] = sc.nextLine();
        }
        System.out.println("Результат: ");
        for (int i = 0; i < n; i++) {
            if (i != n - 1)
                System.out.print(str[i] + ", ");
            else
                System.out.print(str[i]);
        }
    }
}

