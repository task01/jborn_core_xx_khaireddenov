package s03;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите размер последовательности: \n");
        int n = input.nextInt();
        int[] array = new int[n];
        System.out.println("Сгенерированная последовательность: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) Math.round(Math.random() * 9);
            System.out.print(array[i]);
        }
    }
}
