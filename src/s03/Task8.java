package s03;

import java.util.Scanner;
import java.lang.StringBuilder;
public class Task8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите строку: ");
        String original = scan.next();
        char[] charArray = original.toCharArray();
        StringBuilder result = new StringBuilder();
        for (int i = charArray.length - 1; i >= 0; i--) {
            result.append(charArray[i]);
        }
        System.out.println("Полученный результат: " + result);
    }
}
