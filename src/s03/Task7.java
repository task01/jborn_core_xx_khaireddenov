package s03;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите слово: ");
        String original=scan.next();;
        StringBuffer newStr=new StringBuffer(original);
        for(int i = 0; i < original.length(); i++) {
            if(Character.isLowerCase(original.charAt(i))) {
                newStr.setCharAt(i, Character.toUpperCase(original.charAt(i)));
            }
            else if(Character.isUpperCase(original.charAt(i))) {
                newStr.setCharAt(i, Character.toLowerCase(original.charAt(i)));
            }
        }
        System.out.println("Полученный результат: " + newStr);
    }
}
