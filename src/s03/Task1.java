package s03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task1 {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите название государства: ");
        String coutry = reader.readLine();
        System.out.println("Введите название столицы: ");
        String capital = reader.readLine();
        System.out.println("Столица государства " + coutry + " город " + capital);
    }
}
