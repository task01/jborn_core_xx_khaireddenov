package s03;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите предложение");
        String str = scanner.nextLine();
        String okzhi = "жи";
        String okshi = "ши";
        String nozhi = "жы";
        String noshi = "шы";
        str = str.toLowerCase();
        if (str.contains(nozhi) || str.contains(noshi)) {
            System.out.println("Неверно. Жи/Ши пиши с буквой 'и'");
        } else if (str.contains(okzhi) || str.contains(okshi)) {
            System.out.println("Верно");
        } else
            System.out.println("Для данного слова не рассматривается");

    }
}
