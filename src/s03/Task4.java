package s03;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите А (высота): ");
        int a = input.nextInt();
        System.out.println("Введите Б (ширина): ");
        int b = input.nextInt();
        System.out.println("Результат: ");
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                if(inRectangle(i, j, a, b)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static boolean inRectangle(int i, int j, int n, int m) {
        return i == 0 || i == n - 1 || j == 0 || j == m - 1;
    }
}