package s03;

import java.util.Scanner;


public class Task5 {
    public static boolean isPalindrome(String s) {
        for (int i = 0, j = s.length() - 1; i < j; i++, j--) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите слово");
        String s = scan.next();
        if (isPalindrome(s)) {
            System.out.println("Введенное слово является полиндромом");
        } else {
            System.out.println("Введенное слово не является полиндром");
        }
    }
}
