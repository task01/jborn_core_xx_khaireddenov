package s03;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите строку: ");
        String stroka = (sc.nextLine());
        System.out.println("Результат: ");
        for (int i = 0; i < stroka.length(); i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(stroka.charAt(i));
            }
            System.out.println();
        }
    }
}
