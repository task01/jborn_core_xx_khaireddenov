import java.util.*;

public class Task3 {
    public static int fib(int n) {
        if (n <= 1) {
            return n;
        }
        return fib(n - 1) + fib(n - 2);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите число:  ");
        int n = input.nextInt();
        System.out.println("n-ое число Фибоначчи = " + fib(n));
    }
}

